FROM node:15-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm ci --only=production

COPY . /usr/src/app

# ${admin_token}
CMD ["sh", "-c", "node index.js"]