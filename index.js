const Discord = require("discord.js");
const { ReactionCollector } = require("discord.js");
const client = new Discord.Client();
require("dotenv").config();

// Admins array
// NOT A PERSISTENT SOLUTION!
// Database should be used to store IDs
var admins = [];

// Get arguments from the command line (bot and admin token from GitLab secrets)
var bot_token = process.env.BOT_TOKEN || null; // Must be 1st argument for Node
var admin_token = process.env.ADMIN_TOKEN || null; // Must be 2nd argument for Node

try {
  console.log("ARGUMENTS ARE:");
  console.log(process.env);
  console.log(`Bot token is ${bot_token} and admin token is ${admin_token}`);
  client.login(`${bot_token}`);
} catch (err) {
  console.log(`Bot token is ${bot_token} and admin token is ${admin_token}`);
  console.log(err);
}

client.on("ready", () => {
  try {
    admins.push(admin_token.toString());
  } catch (err) {
    console.log(err.message);
  }
  console.log(`Logged in as ${client.user.tag}!`);
});

// Prefix to the commands
const prefix = ".";

// TODO: change message checking to switch case (faster and better solution)
client.on("message", async (message) => {
  if (message.content === `${prefix}ping`) {
    message.reply("Pong!");
  }

  if (message.content.startsWith(`${prefix}hello there`)) {
    message.channel.send(`General <@!${message.author.id}>`);
  }

  if (message.content === `${prefix}avatar`) {
    // Send the user's avatar URL
    message.reply(message.author.displayAvatarURL());
  }

  if (message.content.startsWith(`${prefix}say `)) {
    const arg = message.content.slice(`${prefix}say `.length).trim().split(" ");
    message.channel.send(`You said ${arg}`);
  }

  if (message.content === `${prefix}test`) {
    const sentMessage = await message.reply(`Testing what?`);
    try {
      message.react("⁉");
    } catch (err) {
      const errMessage = await message.channel.send(
        `Something went wrong:\n${err.message}`
      );
      console.log(error.message);
      await errMessage.delete({ timeout: 10000 });
    }
    await sentMessage.delete({ timeout: 4000 });
    await message.delete();
  }

  // The story of Darth Plagueis The Wise.
  if (message.content === `${prefix}itsnotastoryajediwouldtellyou`) {
    try {
      const sentMessage = await message.reply(
        `Did you ever hear the tragedy of Darth Plagueis The Wise? I thought not. It’s not a story the Jedi would tell you. It’s a Sith legend. Darth Plagueis was a Dark Lord of the Sith, so powerful and so wise he could use the Force to influence the midichlorians to create life... He had such a knowledge of the dark side that he could even keep the ones he cared about from dying. The dark side of the Force is a pathway to many abilities some consider to be unnatural. He became so powerful… the only thing he was afraid of was losing his power, which eventually, of course, he did. Unfortunately, he taught his apprentice everything he knew, then his apprentice killed him in his sleep. Ironic. He could save others from death, but not himself.`
      );
      await message.delete({ timeout: 500 });
      sentMessage.react("💔");
      sentMessage.react("😭");
    } catch {
      console.log(error.message);
    }
  }

  // Add user to admins by mention
  // ONLY SAVES ADMIS TO RUNTIME ARRAY
  // SO IF BOT RESTARTS THE ARRAY IS LOST
  // Might implement something better later
  if (message.content.startsWith(`${prefix}admin `)) {
    // Splitting the message (there must be a better way)
    const withoutPrefix = message.content.slice(prefix.length);
    const split = withoutPrefix.split(/ +/);
    const args = split.slice(1);

    const timeout = 4500; // timeout time for deleting messages

    if (args[0]) {
      const user = getUserFromMention(args[0]);
      if (!user) {
        return message.reply("Tag someone pls.");
      }
      if (admins.includes(user.id)) {
        const delMess = await message.channel.send(
          `${user} is already an admin.`
        );
        await message.delete({ timeout });
        return await delMess.delete();
      }
      if (admins.includes(`${message.author.id}`)) {
        admins.push(user.id);
        // console.log(admins);
        return message.channel.send(`<@!${user.id}> is now an admin.`);
      }
      const delMess = await message.channel.send(
        `<@!${user.id}> you don't have the rights to do this.`
      );
      await message.delete({ timeout });
      return await delMess.delete();
    }

    return message.channel.send(`This shouldn't happen...`);
  }

  // Delete messages
  if (message.content.startsWith(`${prefix}delete `)) {
    if (admins.includes(`${message.author.id}`)) {
      const args = message.content.split(" ").slice(1); // All arguments behind the command name with the prefix
      const amount = args.join(" "); // Amount of messages which should be deleted

      if (!amount)
        return message.reply("Please give amount of messages to delete."); // Checks if the `amount` parameter is given
      if (isNaN(amount)) return message.reply("Give only numbers. :smile:"); // Checks if the `amount` parameter is a number. If not, the command throws an error

      if (amount > 100)
        return message.reply(
          "No more than 100 messages can be deleted at a time."
        ); // Checks if the `amount` integer is bigger than 100
      if (amount < 1)
        return message.reply("You must delete at least 1 message"); // Checks if the `amount` integer is smaller than 1

      await message.delete({ timeout: 100 });

      const amountNUM = Number(amount);

      await message.channel.messages
        .fetch({ limit: amountNUM })
        .then(async (messages) => {
          // Fetches the messages
          message.channel.bulkDelete(
            messages // Bulk deletes all messages that have been fetched and are not older than 14 days (due to the Discord API)
          );
          const sentMessage = await message.channel.send(
            `Deleted ${amount} messages!`
          );
          await sentMessage.delete({ timeout: 3000 });
        })
        .catch((err) => console.log(err.message));
    } else {
      const delMess = await message.channel.send(
        `<@!${user.id}> you don't have the rights to do this.`
      );
      await message.delete({ timeout });
      return await delMess.delete();
    }
  }
});

/**
 * @description Helper function to get mentioned user.
 * @param {string} mention First mentioned user from message.
 * @returns {user} Discord user object.
 */
function getUserFromMention(mention) {
  if (!mention) return;

  if (mention.startsWith("<@") && mention.endsWith(">")) {
    mention = mention.slice(2, -1);
    if (mention.startsWith("!")) {
      mention = mention.slice(1);
    }
    return client.users.cache.get(mention);
  }
}
